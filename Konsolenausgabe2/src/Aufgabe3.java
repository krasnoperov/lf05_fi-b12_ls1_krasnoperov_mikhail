
public class Aufgabe3 {

	public static void main(String[] args) {
		System.out.printf("Fahrenheit | %8s %n","Celsius");
		System.out.println("----------------------");
		System.out.printf("-20 %8s %8.2f %n", "|", -28.88889);
		System.out.printf("-10 %8s %8.2f %n", "|", -23.33333);
		System.out.printf("+0 %9s %8.2f %n", "|", -17.7778);
		System.out.printf("+20 %8s %8.2f %n", "|", -6.6667);
		System.out.printf("+30 %8s %8.2f %n", "|", -1.1111);
		

	}

}
