
public class Aufgabe2 {

	public static void main(String[] args) {
		System.out.printf("0! %5s %19s %4s  %n", "=","=", "1");
		System.out.printf("1! %5s 1 %17s %4s  %n", "=","=", "1");
		System.out.printf("2! %5s 1 * 2 %13s %4s  %n", "=","=", "2");
		System.out.printf("3! %5s 1 * 2 * 3 %9s %4s  %n", "=","=", "6");
		System.out.printf("4! %5s 1 * 2 * 3 * 4 %5s %4s  %n", "=","=", "24");
		System.out.printf("5! %5s 1 * 2 * 3 * 4 * 5 %s %4s  %n", "=","=", "120");

	}

}
